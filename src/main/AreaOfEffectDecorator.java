package main;

public class AreaOfEffectDecorator extends SpellDecorator
{
    public AreaOfEffectDecorator(Spell spell)
    {
        super(spell);
    }

    @Override
    public void castSpell()
    {
        System.out.println("Applying area-of-effect effect to the " + spell.getDescription());
        spell.castSpell();
    }

    @Override
    public String getDescription()
    {
        return spell.getDescription() + " + Area of Effect";
    }
}