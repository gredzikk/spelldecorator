package main;

public class FireDamageDecorator extends SpellDecorator
{
    public FireDamageDecorator(Spell spell)
    {
        super(spell);
    }

    @Override
    public void castSpell()
    {
        System.out.println("Adding fire damage to the " + spell.getDescription());
        spell.castSpell();
    }

    @Override
    public String getDescription()
    {
        return spell.getDescription() + " + Fire Damage";
    }
}