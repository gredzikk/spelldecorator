package main;

public class FrostDamageDecorator extends SpellDecorator
{
    public FrostDamageDecorator(Spell spell)
    {
        super(spell);
    }

    @Override
    public void castSpell()
    {
        System.out.println("Adding frost damage to the " + spell.getDescription());
        spell.castSpell();
    }

    @Override
    public String getDescription()
    {
        return spell.getDescription() + " + Frost Damage";
    }
}