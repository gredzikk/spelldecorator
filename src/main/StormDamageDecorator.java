package main;

public class StormDamageDecorator extends SpellDecorator
{
    public StormDamageDecorator(Spell spell) { super(spell); }

    @Override
    public void castSpell()
    {
        System.out.println("Adding storm damage to the " + spell.getDescription());
        spell.castSpell();
    }

    @Override
    public String getDescription()
    {
        return spell.getDescription() + " + Storm Damage";
    }
}