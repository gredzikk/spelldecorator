package main;

public class BaseSpell implements Spell
{
    private final String spellType;

    public BaseSpell(String spellType)
    {
        this.spellType = spellType;
    }
    @Override
    public void castSpell()
    {
        System.out.println("Casting " + spellType + " spell.");
    }

    @Override
    public String getDescription()
    {
        return spellType + " spell";
    }
}