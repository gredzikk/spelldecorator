package main;

public interface Spell
{
    void castSpell();
    String getDescription();
}
