package main;

public class Main
{
    public static void main(String[] args)
    {
        Spell baseSpell = new BaseSpell("Basic");

        Spell fireSpell = new FireDamageDecorator(new AreaOfEffectDecorator(baseSpell));
        fireSpell.castSpell();
        System.out.println(fireSpell.getDescription());
        System.out.println();

        Spell frostSpell = new FrostDamageDecorator(new BaseSpell("Fire"));
        frostSpell.castSpell();
        System.out.println(frostSpell.getDescription());
        System.out.println();

        Spell stormSpell = new StormDamageDecorator(new BaseSpell("Air"));
        stormSpell.castSpell();

    }
}