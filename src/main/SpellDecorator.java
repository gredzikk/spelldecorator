package main;

public abstract class SpellDecorator implements Spell
{
    protected Spell spell;

    public SpellDecorator(Spell spell)
    {
        this.spell = spell;
    }

    @Override
    public void castSpell()
    {
        spell.castSpell();
    }

    @Override
    public String getDescription()
    {
        return spell.getDescription();
    }
}